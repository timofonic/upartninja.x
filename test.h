#ifndef TESTNINJA_H
#define TESTNINJA_H

#include "globals.h"


#define LOW 0
#define HIGH 1

//////TESTING CONSTANTS/////////////////
///for easier tunning purpses//////////
//if the voltage drops by 100 high side read will be 923,low side will be 100
#define CP_HIGH 	900	//V drop for HIGH side conductivity test
#define CP_LOW  	123	//V drop for LOW side conductivity test

#define CAP_DIFF 	10	//by how much will a CAP charge over noise...
#define RES_DD 		170	//by how much will zener Vdrop change with diff resistors
#define ZENER_DD 	100	//by how much will 2 anti paraller diodes VDs differ 
#define FET_HIGH        923
#define FET_LOW         100
#define TEST_DELAY    	200
#define DARL_BJT        50

#define CAP_LOWCAP      200    //Capacitors of lower values are charged with 470K resistor to get more accuracy

//Enum with the different part type names...

typedef enum type {
    ERROR = 0, NFET, PFET, NMOS, PMOS, NPN, CA, PNP, CC,
    DIODE, TRIAC, SCR, CAP, RES, ZENER, DD, NPN_D, PNP_D, 
    NOID, ERROR1, ERROR2, ERROR3, ERROR4, ERROR5, ERROR6
} type;


u8 testPart(void);

u16 checkConduct(u8 A, u8 B, u8 test);
void testConduct(void);

type getPartSS(void);
type getPartSS_2nodes(void);
type getPartSS_CAP_RES(void);
type getPartSS_FET(void);
type getPartSS_Darlington_BJT(void);
type getPartSS_NPN(void);
type getPartSS_PNP(void);

//main called for further annalysis based on part subsection
u16 switchPart(type SS);


//These functions test the  parts
u16 testNFET(void);
u16 testPFET(void);
u16 testNMOS(void);
u16 testPMOS(void);
u16 testNPN(void);
u16 testCA(void);
u16 testPNP(void);
u16 testCC(void);
u16 testDIODE(void);
u16 testTRIAC(void);
u16 testSCR(void);
u16 testCAP(void);
u16 testRES(void);
u16 testZENER(void);
u16 testDD(void);
u16 testNPN_D(void);
u16 testPNP_D(void);

//stes up the ADC module and takes a reading of the Pin
u16 ReadADC(u8 Pin);
//only sets up the ADC, for future fast acquisitions
void quickADCsetup(u8 Pin);

//conects the pin to state(HIGH/LOW) through a 0,680,or 470k resistor
void R_680(u8 Pin, u8 State);
void R_470K(u8 Pin, u8 State);
void R_0(u8 Pin, u8 State);
//Brings the Pin into high impedance mode(input)
void HiZ(u8 Pin);
//Brings pins A,B, and C into HiZ
void HiZ3(u8 A, u8 B, u8 C);

//Output results to LCD, serial or USB-CDC
void printPart_lcd(void);

void printPart_cdc(void);
void tListPrint_cdc(void);

void WritePinout_serial(void);
void printPart_serial(void);
void tListPrint_serial(void);

void putcUSART(char data);
void putrsUSART(const rom char *data);
void puts_cdc(rom char *s);
void putINT_cdc(u16 value);
void putINT_serial(u16 value);
void putINT_lcd(u16 value);

void initT(void);
void Delay_MS(u8 s);
double mylog(u16 value);

////////////////////////////////////////////////////
//Global Variables//////////////////////////////////
extern u8 terminalF;
extern u16 VCC;
extern u16 VCC_2;
#endif