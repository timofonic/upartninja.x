#include "globals.h"
#include "config.h"
#include "HD44780.h"
#include "test.h"
#include "main.h"

//USB stack
#include "dp_usb\usb_stack_globals.h"    // USB stack only defines Not function related.
#include "descriptors.h"	// JTR Only included in main.c


u8 usbCON = 0; //usb connection flag, if connected 1;
u8 terminalF = 1;
u16 VCC = 5000;
u16 VCC_2 = 5000;

#pragma udata
extern BYTE usb_device_state;

#pragma code
void testHW(){
    u16 q2500;

    //Calculate Vcc measuring the VREF that is accurate 2.5V
    ADCON2 = 0b10111110; //R justified result, 20TAD time 111, FOSC/64 110
    ADCON1 = 0b00001000; //Internal Vrefs VDD/VSS, A0-6 analog
    ADCON0 = 0b00001111; //channel AN3, GO/!DONE=1, ADON=1
    while (ADCON0 & 0b10);
    q2500 = ADRES;

    VCC = 2557500/q2500; //Vcc[mV] = 1023*2500/Q[2.5V]
    VCC_2 = VCC/2;
}

void main(void) {
    BYTE RecvdByte;
    
    init(); //setup the crystal, pins

#ifdef HAS_SERIAL_PORT
    initSerial();
#endif

#ifdef HAS_USB_CDC
    initUSB_CDC();
#endif

#ifdef HAS_LCD
    initLCD();
#endif

    while (1) {
        testHW();
        testPart();
#ifdef HAS_USB_CDC
        if (usbCON) {
            CDC_Flush_In_Now();
            if (poll_getc_cdc(&RecvdByte)) {
                CDC_Flush_In_Now();
                cdc_switch(RecvdByte);
            }
        }
#endif
    }
}//end main

static void init(void) {
    //unsigned int cnt = 2048;

    //all pins digital
    //disable some defaults
    ADCON1 |= 0b1111; //all pins digital
    CVRCON = 0b00000000;

    //make sure everything is input (should be on startup, but just in case)
    TRISA = 0b11111111;
    TRISB = 0b11111111;
    TRISE = 0b11111111;
    TRISC = 0b11111111;

    //comparator analog pins setup...
    CMCONbits.C2INV = 1; //whne voltage on RA1 > vreff cout2=>1
    CMCONbits.C1INV = 1; //whne voltage on RA1 > vreff cout1=>1
    CMCONbits.CIS = 0; //the Cvin- input aer RA0 na RA1
    CMCONbits.CM2 = 1, CMCONbits.CM1 = 0, CMCONbits.CM0 = 0; //100 C1 and C2 V+ conencted to vref, while v+ conected to RA1 and RA2
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    IPR2bits.CMIP = 1;

    //on 18f24j50 we must manually enable PLL and wait at least 2ms for a lock
    //OSCTUNEbits.PLLEN = 1;  //enable PLL
    //while(cnt--); //wait for lock
}

void initUSB_CDC(){
    u32 usbTimerPN = 0;

    initCDC(); // setup the CDC state machine
    
    usb_init(cdc_device_descriptor, cdc_config_descriptor, cdc_str_descs, USB_NUM_STRINGS); // initialize USB. TODO: Remove magic with macro
    usb_start(); //start the USB peripheral

    #ifdef USB_INTERRUPTS
        EnableUsbPerifInterrupts(USB_TRN + USB_SOF + USB_UERR + USB_URST);
    #if defined __18CXX //turn on interrupts for PIC18
        INTCONbits.PEIE = 1;
        INTCONbits.GIE = 1;
    #endif
        EnableUsbGlobalInterrupt(); // Only enables global USB interrupt. Chip interrupts must be enabled by the user (PIC18)
    #endif

    // Wait for USB to connect
    usbCON = 1;
    do {
        usbTimerPN++;
    #ifndef USB_INTERRUPTS
        usb_handler();
    #endif
        if (usbTimerPN > 1000000) {
            usbCON = 0;
            break;
        }
    } while (usb_device_state < CONFIGURED_STATE);

    if (!usbCON) usb_register_sof_handler(CDCFlushOnTimeout); // Register our CDC timeout handler after device configured

    Delay_MS(10);
    CDC_Flush_In_Now();
}

#ifdef HAS_LCD
void initLCD(){
    HD44780_Reset(); //setup the LCD
    HD44780_Init();
    //LCD_Backlight(1);//turn it on, we ignore the parameter

    LCD_CursorPosition(0);
    LCD_WriteString("Part Ninja r1931");
    LCD_CursorPosition(21);
    LCD_WriteString("      testing...");
}
#endif //#ifdef HAS_LCD

void initSerial(){
    //Configure TX serial
    TXSTAbits.BRGH = 1; //High speed
    TXSTAbits.SYNC = 0; //Asynchronous mode
    TXSTAbits.TX9  = 0; //Selects 8-bit transmission
    TXSTAbits.TXEN = 1; //Transmit enabled

    BAUDCONbits.BRG16 = 1; //16-bit Baud Rate Generator

    // Baud Rate = FOSC/[4 (n + 1)]
    // 48e6/(4(10+1)) = 113.636kbps
    SPBRGH = 0;
    SPBRG = 103;

    RCSTAbits.SPEN = 1; //Serial port enabled

    putrsUSART("Part Ninja\n");
}

void USBSuspend(void) {
    
}

u8 cdc_switch(BYTE rB) {
    switch (rB) {
        case 't':
            terminalF = 1;
            return 1;
        case 'p':
            terminalF = 0;
            return 1;
        default:
            return 0;
    }
}


//PIC18F style interrupts with remapping for bootloader
//	Interrupt remap chain
//
//This function directs the interrupt to
// the proper function depending on the mode
// set in the mode variable.
//USB stack on low priority interrupts,
#pragma interruptlow InterruptHandlerLow nosave= PROD, PCLATH, PCLATU, TBLPTR, TBLPTRU, TABLAT, section (".tmpdata"), section("MATH_DATA")

void InterruptHandlerLow(void) {

    //NOT USED!!, but halt here if for some reason is called
    while(1);
}

#pragma interrupt InterruptHandlerHigh nosave= PROD, PCLATH, PCLATU, TBLPTR, TBLPTRU, TABLAT, section (".tmpdata"), section("MATH_DATA")

void InterruptHandlerHigh(void) { //Also legacy mode interrupt.

    if (CMP_INTF) { //Comparator interrupt
        T0CONbits.TMR0ON = 0;
        CMP_INTE = 0;
        CMP_INTF = 0;
        INTCONbits.TMR0IE = 0;
        INTCONbits.TMR0IF = 0;
        testCMPCount += TMR0L;
        testCMPCount += ((u16)TMR0H<<8);
        testCMP = 0;
    }
    if (INTCONbits.TMR0IF){ //Timer0 interrupt
        INTCONbits.TMR0IF = 0;
        testCMPCount += 65536;
    }
    if (PIR2bits.USBIF){ //usb interrupt
        usb_handler();
        ClearGlobalUsbInterruptFlag();
    }
}

//these statements remap the vector to our function
//When the interrupt fires the PIC checks here for directions
#pragma code REMAPPED_HIGH_INTERRUPT_VECTOR = REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS

void Remapped_High_ISR(void) {
    _asm goto InterruptHandlerHigh _endasm
}

#pragma code REMAPPED_LOW_INTERRUPT_VECTOR = REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS

void Remapped_Low_ISR(void) {
    _asm goto InterruptHandlerLow _endasm
}

//relocate the reset vector
extern void _startup(void);
#pragma code REMAPPED_RESET_VECTOR = REMAPPED_RESET_VECTOR_ADDRESS

void _reset(void) {
    _asm goto _startup _endasm
}
//set the initial vectors so this works without the bootloader too.
#pragma code HIGH_INTERRUPT_VECTOR = 0x08

void High_ISR(void) {
    _asm goto REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS _endasm
}
#pragma code LOW_INTERRUPT_VECTOR = 0x18

void Low_ISR(void) {
    _asm goto REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS _endasm
}
