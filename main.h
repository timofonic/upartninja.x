/* 
 * File:   newfile.h
 * Author: cmr_user
 *
 * Created on 5 de enero de 2014, 16:33
 */

#ifndef MAIN_H
#define	MAIN_H

#include "globals.h"

#define REMAPPED_RESET_VECTOR_ADDRESS		0x800
#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS	0x808
#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS	0x818

void testHW(void);
static void init(void);
void initSerial(void);
void initUSB_CDC(void);
void initLCD(void);
void InterruptHandlerHigh();
void InterruptHandlerLow();
void USBSuspend(void);
u8 cdc_switch(BYTE rB);

////////////////////////////////////////////////////
//Global Variables//////////////////////////////////
extern u8 testCMP;
extern u32 testCMPCount;
#endif	/* NEWFILE_H */

