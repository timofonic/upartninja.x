#!/usr/bin/python

import serial 
import time
import struct

if __name__ == '__main__':
    BAUD = 115200
    #PORT = "/dev/ttyAMA0"
    PORT = "COM10"
    TIMEOUT = 0.5

    s = serial.Serial(PORT, baudrate=BAUD, timeout = TIMEOUT ) 
    
    while(1):
        data = s.read(1024)
        if data:
            print data
