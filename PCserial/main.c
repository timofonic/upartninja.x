#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#define SPORT  "\\\\.\\COM10"
#define BUFF_SIZE 1024

enum type {TERROR=0,NFET,PFET,NMOS,PMOS,NPN,CA,PNP,CC,
	DIODE,TRIAC,SCR,CAP,RES,ZENER,DD,NPN_D,PNP_D,NOID=20,
	ERROR1,ERROR2,ERROR3,ERROR4,ERROR5,ERROR6} ;

char* cnames[] = {
    "ERROR","NFET","PFET","NMOS","PMOS","NPN","CA","PNP","CC",
	"DIODE","TRIAC","SCR","CAP","RES","ZENER","DD","NPN_D","PNP_D","NOID",
    "ERROR1","ERROR2","ERROR3","ERROR4","ERROR5","ERROR6"
    };

void ErrorExit(LPTSTR lpszFunction){
    // Retrieve the system error message for the last-error code
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError();

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );

    // Display the error message and exit the process

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
        (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
    wsprintf((LPTSTR)lpDisplayBuf,
        TEXT("%s failed with error %d: %s"),
        lpszFunction, dw, lpMsgBuf);
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    ExitProcess(dw);
}

int main(){
    HANDLE Port;
    COMMCONFIG configs_comm;
    DWORD nSize;
    uint8_t input[BUFF_SIZE];
    

    Port = CreateFile(SPORT, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
    if(Port==INVALID_HANDLE_VALUE){
        ErrorExit("CreateFile");
    }
    
    if(!GetCommConfig(Port, &configs_comm, &nSize)){
        ErrorExit("GetCommConfig");
    }

    configs_comm.dcb.BaudRate = 115200;//2457600;1228800;921600;460800;230400;115200;
    configs_comm.dcb.fParity = 0;
    configs_comm.dcb.fOutxCtsFlow = 0;
    configs_comm.dcb.fOutxDsrFlow = 0;
    configs_comm.dcb.fOutX = 0;
    configs_comm.dcb.fInX = 0;
    configs_comm.dcb.ByteSize = 8;
    configs_comm.dcb.Parity = NOPARITY;
    configs_comm.dcb.StopBits = ONESTOPBIT;

    //if(!CommConfigDialog(PUERTO, NULL, &configs_comm)){
    //    ErrorExit("CommConfigDialog");
    //}

    if(!SetCommConfig(Port, &configs_comm, sizeof(configs_comm))){
        ErrorExit("SetCommConfig");
    }

    //WriteFile(Port, "t", 1, &nSize, NULL);


    while(ReadFile(Port, input, BUFF_SIZE, &nSize, NULL)){
        if(nSize){
            //printf("{{%d}}", nSize);
            fwrite(input, nSize, 1, stdout);
        }
    }

    CloseHandle(Port);
}

