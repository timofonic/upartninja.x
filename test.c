#include "test.h"
#include "HD44780.h"
#include "dp_usb/cdc.h"
#include <stdlib.h>

//Global variable initialisation

//The list where all the conductive paths are kept.
//[0]->[1] pins for direction, [3] is the read value r680
//5v--[680]--[0]-->[1]---GND
u16 tList[12][3] = {
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0}};
u8 tIN[3] = {0, 0, 0}; //How many times there is conduction when port (i) is 5v
u8 tOUT[3] = {0, 0, 0}; //How many times there is conduction when port (i) is GND
u8 tC[3] = {0, 0, 0}; //How many times there is conduction when port (i) is used (5v or GND)
u8 tN[8] = {0, 0, 0, 0, 0, 0, 0, 0}; //Number of pins that have contributed i+1 times.
    //Ex. Diode tN[0] = 2, two pins contribute to create 1 conduction path
    //Ex. Resistor tN[1] = 2, two pins contribute to create 2 conduction paths
    //Ex. NPN Transistor tN[0]=2, tN[1]=1, two pins 1 conduction path (E,C); one pin 2 conduction paths (B)
u8 nC = 0; //Number of connection pathes
u8 diff; //Number of paths are different when the unconnected 3rd pin was charged to HIGH or LOW prior to test

u8 node; //node is the pins common to two conduction paths
u8 testCMP = 0; //Marks when pin crosses 2.5V threshold
u32 testCMPCount = 0; //Counts the number of CLK/4 when testCMP was enabled

//Guessed component and characteristics
type PartSS; //Part type
char pins[3] = {0, 0, 0}; //Pin description, Ex. Trasnsistor: E,B,C
u16 part_val; //Part value in (part_unit) units
char part_unit; //Part unit of measure, may include exponent, Ex. Resistor: R or K

rom const char *rom type_descs[] = {
    "Error",
    "N-JFET rON:",
    "P-JFET rON:",
    "NMOS   rON:",
    "PMOS   rON:",
    "NPN    hFE:",
    "DD-CA   Vd:",
    "PNP    hFE:",
    "DD-CC   Vd:",
    "DIODE   Vd:",
    "TRIAC      ",
    "SCR     vG:",
    "Capacitor :",
    "Resistor  :",
    "ZENER   Vz:",
    "DD-Anti Vd:",
    "NPN-Dar Vd:",
    "PNP-Dar Vd:",
    "NOID", "ERROR1", "ERROR2", "ERROR3", "ERROR4", "UNCONNECTED", "ERROR6"
};


//Encapsulates the whole testing process
u8 testPart() {
    PartSS = 0;
    diff = 0;
    part_unit = 'm'; //mV by default
    pins[0] = 'X';
    pins[1] = 'X';
    pins[2] = 'X';

    //returns the number of conducting directions between all 3 pins
    //On return tList, nC, diff, and other variables hold the results
    testConduct();

    //gets the ID of the part that is most probable
    PartSS = getPartSS(); 

    //switches depending on the PartSS value, here is where all the part specific functions all called
    part_val = switchPart(PartSS);

#ifdef HAS_SERIAL_PORT
    putrsUSART("\n\n");
    tListPrint_serial();
    putrsUSART("\nPartSS: ");
    putINT_serial(PartSS);
    putcUSART('\n');
    printPart_serial();
#endif

#ifdef HAS_USB_CDC
    if (terminalF) {
        puts_cdc("\n\n");
        tListPrint_cdc();
        puts_cdc("\nPartSS: ");
        putINT_cdc(PartSS);
        putc_cdc('\n');
        printPart_cdc();
    }
#endif

#ifdef HAS_LCD
    printPart_lcd();
#endif

    if (PartSS) return 1;
    return 0;
}


//*********************************************************************//
//   Functions to guess the conduction paths, used to part recognition
//*********************************************************************//

//Checks conductivity through form [A]->[B] while keeping the 3rd pin at the test state briefly before the test.
//5V--[680]-Vt-[A]-->[B]---GND, if Vt < 93% VCC it is conducting.
//Returns the Value of Vt
//The test is made just after the 3rd pin is charged to test State, and then made HiZ
//this is done so that FET devices can have their Gates still charged, while it makes no diff to other parts
//Triacs, and SCRs are an exception, here the short trigering of the gate will make a differense, but it is different to the Fets...

u16 checkConduct(u8 A, u8 B, u8 state) {
    u8 i = TEST_DELAY;
    u8 C; //Calculate the leftover pin
    u16 Value;
    C = 3 - (A + B);

    //charges the leftover pin to test State and waits for charge
    R_680(C, state); 
    Delay_MS(10);

    quickADCsetup(B); //sets up the ADC for a later reading

    //conects the A pin to 5V throgh 680R 5V--[680]--[A]
    //brings the leftover pin to HiZ
    R_680(B, LOW);
    R_0(A, HIGH); 
    HiZ(C);
    while (i--);

    //Do the ADC acq just after the left over pin is HiZ
    ADCON0 |= 0b10; //GO/!DONE=1
    while (ADCON0 & 0b10);
    Value = 1023 - ADRES;

    //Discharge?
    R_0(A, LOW);
    Delay_MS(10);
    R_0(B, LOW);
    Delay_MS(10);
    HiZ(A);
    HiZ(B);

    //If the read value is less the 93% of VCC then it is a conductor
    if (Value < CP_HIGH) return Value + 1; 
    return 0; //otherwise its not conducting
}

//Runs twice through all 6 posile conductive paths between 3 pins,
//once while keeping the 3rd pin LOW briefly before the test,
//second while HIGH, the pin is at HiZ during the test.
//Returns the number of conductive paths...
//If the they don't change in both passes it returs the number for the first pass, diff=0
//if they do change, returns diff and the full number of conductive paths...
//also diff contains the information on the nC for the first pass. diff = nC+1

void testConduct() {
    u16 Value;
    u8 i, j, Cstate;
    u8 num_cond1;
    initT();
    nC = 0;
    diff = 0;

    //Loop over all pin combinations with Cstate (3rd pin state) (0 for LOW, 1 for High)
    for (Cstate = 0; Cstate < 2; Cstate++){
        //loop that covers all posible pin combinations
        for (i = 0; i < 3; i++)
            for (j = 0; j < 3; j++){
                if (i==j) continue;
                if (Value = checkConduct(i, j, Cstate)) {
                    tList[nC][0] = i;
                    tList[nC][1] = j;
                    tList[nC++][2] = Value;
                    tIN[i] += 1;
                    tOUT[j] += 1;
                    tC[i] += 1;
                    tC[j] += 1;
                }
            }

        //Store the nc of the first pass
        if (!Cstate) num_cond1 = nC;
    }

    if (nC == 0) return; //if no CPs return 0, and exit function

    if (nC == (num_cond1 * 2)){
        //If the Nc for both passes is the same go through the list
        //and check if any CP is different between the passes
        for (i = 0; i < num_cond1; i++) {
            if ((tList[i][0] != tList[i + num_cond1][0]) || (tList[i][1] != tList[i + num_cond1][1])){
                //If it finds at list one diff=num_cond1+1 and return the full number of CPs
                diff = num_cond1 + 1; 
                break;
            }
            else {
                //If it doesn't find any difference from the two passes
                //diff remains 0, and only the nC of the first pass is returned
                for (i = 0; i < 3; i++) {
                    tIN[i] /= 2;
                    tOUT[i] /= 2;
                    tC[i] /= 2;
                }
                nC = num_cond1;
            }
        }
    }
    else{
        //If the number of CPs in the two passes is different return the full number of CPs
        diff = num_cond1 + 1;
    }

    for (i = 1; i < 9; i++)
        for (j = 0; j < 3; j++)
            if (tC[j] == i) tN[i - 1] += 1;
}

//*********************************************************************//
//          Functions to guess the connected component
//*********************************************************************//

///Returns the code for the tested part



type getPartSS() {
    u8 num_nodes = 0; //nodes are pins common to various conduction paths
    u8 cond_direction = 0; //Direction of current flow  (0:none, 1:IN, 2:OUT)

    //If connection paths are not found do a further analysis
    if (nC == 0) return getPartSS_CAP_RES();

    //diff present in the HighZ 3rd pin test, must be FET
    if (diff) return getPartSS_FET();

    //*** Next tests are for devices where there is no diff between the two passes (diff=0) ***//

    //one conduct, must be Diode
    if (nC == 1) return DIODE;

    //three conducts, must be Darlington BJT
    if (nC == 3) return getPartSS_Darlington_BJT();

    //Get the number of nodes, and in case of only one it's direction
    if (tN[1] == 1){
        u8 j, i;
        for (i = 0; i < 3; i++){
            if (tC[i] == 2) {
                if (tIN[i] == 2) {
                    num_nodes = 1;
                    cond_direction = 1;
                    node = i;
                    break;
                }
                if (tOUT[i] == 2) {
                    num_nodes = 1;
                    cond_direction = 2;
                    node = i;
                    break;
                }
            }
        }
    }
    else if ((tN[1] == 2) && (nC == 2)) num_nodes = 2;

    //four conducts and one node, must be FET
    if ((nC == 4) && (num_nodes == 1)) {
        if (cond_direction == 1) return NFET;
        if (cond_direction == 2) return PFET;
    }

    //no posibilities other than 2 conducts, error if other
    if (nC != 2) return ERROR3;

    //2 conducts, 2 nodes, are Res/antiP_D/Zener/Cap
    if (num_nodes == 2) return getPartSS_2nodes();

    //only 1 node combinatons left, error if other
    if (num_nodes != 1) return ERROR4;

    //2conducts, 1 node, direction 1(OUT), must be NPN/DD-CA
    if (cond_direction == 1) return getPartSS_NPN();

    //2conducts, 1 node. directon 2 (IN), muct be PNP/DD-CC
    if (cond_direction == 2) return getPartSS_PNP();

    //no other posibility, error
    return NOID; 
}

//called only if nC=0, so no CP were found in the r680 test...

type getPartSS_CAP_RES() {
    u8 i, j;
    u16 vT1 = 0, vT2, k2;
    for (i = 0; i < 2; i++)
        for (j = i + 1; j < 3; j++) {
            k2 = 400;
            quickADCsetup(i);
            R_0(j, LOW);
            R_470K(i, HIGH);
            while (k2--);

            //Do the ADC acq just after the left over pin is HiZ
            ADCON0 |= 0b10; //GO/!DONE=1
            while (ADCON0 & 0b10);
            vT1 = ADRES;

            //Do the ADC acq after some time and see if the value changes
            Delay_MS(10);
            ADCON0 |= 0b10; //GO/!DONE=1
            while (ADCON0 & 0b10);
            vT2 = ADRES;

            //Discharge?
            R_680(i, LOW);
            Delay_MS(10);
            R_0(i, LOW);
            HiZ(i);
            HiZ(j);

            if (vT1 > CP_HIGH) continue;
            else {
                tList[0][0] = i;
                tList[0][1] = j;
                tList[0][2] = 1023;
                tList[1][0] = j;
                tList[1][1] = i;
                tList[1][2] = 1023;
                nC = 2;
                if (vT2 > (vT1 + CAP_DIFF)) return CAP;
                else return RES;
            }
        }
    return ERROR5;
}

//we know nC==2,nN=2,diff=0,  => bidirectional conduction between only 2 points
//tList[0][0] and tList[0][1];

type getPartSS_2nodes() {
    u32 temp;
    u16 r470K_1, r470K_2, r680_1, r680_2;
    u8 tp1 = tList[0][0], tp2 = tList[0][1]; //loading the pin numbers

    //Getting 2 consecutive reading on the same part, if they change must be a cap
    //charge with 5V through a 470K resistor
    quickADCsetup(tp1);
    R_0(tp2, LOW);
    R_470K(tp1, HIGH);
    Delay_MS(1);

    ADCON0 |= 0b10; //GO/!DONE=1
    while (ADCON0 & 0b10);
    r470K_1 = ADRES;

    Delay_MS(10);

    ADCON0 |= 0b10; //GO/!DONE=1
    while (ADCON0 & 0b10);
    r470K_2 = ADRES;

    //if the difference is larger than noise its a cap
    if (r470K_2 > (r470K_1 + CAP_DIFF)) {
        HiZ(tp1);
        HiZ(tp2);
        return CAP; //voltage increase over time => CAP
    }

    //discharge if anything
    R_680(tp1, LOW); 
    Delay_MS(20);

    //charge with 5V through a 680 resistor and do 2 consecutive reading
    R_680(tp1, HIGH);

    ADCON0 |= 0b10; //GO/!DONE=1
    while (ADCON0 & 0b10);
    r680_1 = ADRES;

    Delay_MS(10);

    ADCON0 |= 0b10; //GO/!DONE=1
    while (ADCON0 & 0b10);
    r680_2 = ADRES;

    //if the difference is larger than noise its a cap
    if (r680_2 > (r680_1 + CAP_DIFF)) {
        HiZ(tp1);
        HiZ(tp2);
        return CAP; //voltage increase over time => CAP
    }

    //If it's not a cap, it may be a resistor, zenner or anti parallel diode
    //invert the pins and repeat the last reading
    quickADCsetup(tp2);
    R_0(tp1, LOW);
    R_680(tp2, HIGH);

    ADCON0 |= 0b10; //GO/!DONE=1
    while (ADCON0 & 0b10);
    r680_2 = ADRES;

    HiZ(tp1);
    HiZ(tp2);

    //if there is a diff larger then spec its a zenner
    if (r680_1 > (r680_2 + ZENER_DD)) return ZENER;
    if (r680_2 > (r680_1 + ZENER_DD)) return ZENER;

    //if there is a difference between the 680 and the 470k series resitors then its a Resistor
    temp = (r680_1 * 100) / r470K_1;
    if (temp > RES_DD) return RES; 

    //if the values are close enough its a anti parallel diode.
    return DD; 
}

type getPartSS_FET() {
    u8 i, j;
    if (nC > 5) {
        if ((tN[5] == 0)) {
            if (tN[4] == 2) {
                for (j = 0; j < 3; j++) {
                    if (tC[j] == 2) node = j;
                }
            } else return ERROR1;
        }
        if (tN[5] == 1) {
            for (j = 0; j < 3; j++) {
                if (tC[j] == 6) node = j;
            }
        } else {
            for (j = 0; j < 3; j++) {
                if (tC[j] != 6) node = j;
            }
        }
        return TRIAC;
    }
    if (nC == 2) {
        if ((diff - 1) == 0) return NFET; //has 2 conducts, high low transition is 0-N-JFET
        else return PFET; //if more then 0 P-JFET
    }
    if (nC == 3) {
        if ((diff - 1) == 1) {
            for (i = 1; i < 3; i++)
                if (tList[0][1] != tList[i][1]) return NMOS; //if different it's NMOS
            return SCR; //if all the second nodes of all 3 conduct are the same its a SCR
        }
        if ((diff - 1) == 2) return PMOS;
    }
    return ERROR2; //untested...uncertain..error
}

type getPartSS_Darlington_BJT(){
    u8 temp;
    if (tN[1] == 3) {
        if (tList[0][2] > tList[1][2]) {
            if (tList[0][2] > tList[2][2]) node = 0;
            else node = 2;
        } else {
            if (tList[1][2] > tList[2][2]) node = 1;
            else node = 2;
        }
        temp = tList[node][1];
        if (tIN[temp] == 1) return NPN_D;
        else return PNP_D;
    } else return ERROR6;
}

//2conducts, 1 node, direction 1(OUT), must be NPN/DD-CA
type getPartSS_NPN() {
    u16 test;

    if (tList[0][2]>(tList[1][2] + DARL_BJT)) {
        node = 0;
        return NPN_D;
    }
    if (tList[1][2]>(tList[0][2] + DARL_BJT)) {
        node = 1;
        return NPN_D;
    }

    R_680(tList[0][0], HIGH); //B
    R_0(tList[1][1], LOW);    //E
    R_680(tList[0][1], HIGH); //C

    test = ReadADC(tList[0][1]);
    HiZ3(tList[0][0], tList[0][1], tList[1][1]);

    //if VCE<5V then NPN
    if (test < CP_HIGH) return NPN;

    //else it's a double diode with CA
    return CA;
}

//2conducts, 1 node. directon 2 (IN), muct be PNP/DD-CC
type getPartSS_PNP() {
    u16 test;

    if (tList[0][2]>(tList[1][2] + DARL_BJT)) {
        node = 0;
        return PNP_D;
    }
    if (tList[1][2]>(tList[0][2] + DARL_BJT)) {
        node = 1;
        return PNP_D;
    }

    R_680(tList[0][1], LOW); //B
    R_0(tList[0][0], HIGH);  //E
    R_680(tList[1][0], LOW); //C

    test = ReadADC(tList[1][0]);
    HiZ3(tList[0][0], tList[1][0], tList[0][1]);

    //if VCE>0V then PNP
    if (test > CP_LOW) return PNP;

    //else it's a double diode with CA
    return CC;
}

//*********************************************************************//
//          Functions parametrize the detected component
//*********************************************************************//
u16 switchPart(type SS) {
    switch (SS) {
        case NFET:
            return testNFET();
        case PFET:
            return testPFET();
        case NMOS:
            return testNMOS();
        case PMOS:
            return testPMOS();
        case NPN:
            return testNPN();
        case CA:
            return testCA();
        case PNP:
            return testPNP();
        case CC:
            return testCC();
        case DIODE:
            return testDIODE();
        case TRIAC:
            return testTRIAC();
        case SCR:
            return testSCR();
        case CAP:
            return testCAP();
        case RES:
            return testRES();
        case ZENER:
            return testZENER();
        case DD:
            return testDD();
        case NPN_D:
            return testNPN_D();
        case PNP_D:
            return testPNP_D();
        default:
            return ERROR;
    }
}

//Test N-JFET, returns the Ron for 5V Vgs, and 5V--[680]-[D][S]--GND
//Pinonut for D and S is unindentifiable...sso both DS and SD will work

u16 testNFET() {
    u8 D, S, G;
    G = node;
    if (G) D = 0;
    else D = 2;
    S = 3 - (G + D);
    pins[G] = 'G';
    pins[D] = 'D';
    pins[S] = 'S';
    return 1;
}

u16 testPFET() {
    u8 D, S, G;
    G = node;
    if (G) D = 0;
    else D = 2;
    S = 3 - (G + D);
    pins[G] = 'G';
    pins[D] = 'D';
    pins[S] = 'S';
    return 1;
}

u16 testNMOS() {
    u32 rON;
    u16 vG, vD;
    u8 D, S, G;
    if (tList[2][0] != tList[0][0]) {
        D = tList[2][0];
        S = tList[2][1];
    } else {
        S = tList[2][0];
        D = tList[2][1];
    }
    G = 3 - (S + D);
    R_680(D, HIGH);
    R_0(S, LOW);
    R_680(G, LOW);
    Delay_MS(10);
    vG = ReadADC(G);
    if (vG > 10) {
        HiZ3(S, D, G);
        return 0; //if gate is lower the vdd it meas current is flowing into it which is not MOS
    }
    vD = ReadADC(D);
    if (vD < 1000) {
        HiZ3(S, D, G);
        return 0; //if the VD is higher then 0 when off means its not PMOS
    }
    R_680(G, HIGH);
    Delay_MS(10);
    vG = ReadADC(G);
    if (vG < 1000) {
        HiZ3(S, D, G);
        return 0; //if gate is higher then 0 it meas current is flowing into it which is not MOS
    }
    vD = ReadADC(D);
    if (vD > 123) {
        HiZ3(S, D, G);
        return 0; //if the VD is lower then 0.9*Vdd when ON means its not PMOS
    }
    HiZ3(S, D, G);
    pins[D] = 'D';
    pins[S] = 'S';
    pins[G] = 'G';
    rON = vD * R680_IDEAL;
    rON = rON / (1023 - vD);
    return (u16) rON;
}

//Tests the part for being PMOS
//first it finds the proper pinout in tList.
//connects the 680R to D LOW, conects S directly to VDD, and Drives the Gate 470K HIGH.
//read both the VG and VD...if VG is~Vdd, and VD is ~0, it brings the VG LOW and reads again..
//This time VG needs to be ~0, and VD~Vdd...if everythinng passes it send the calculated Von resistance at -5V..

u16 testPMOS() {
    u32 rON;
    u16 vG, vD;
    u8 D, S, G;
    S = tList[2][1];
    D = tList[2][0];
    G = 3 - (S + D);
    R_680(D, LOW);
    R_0(S, HIGH);
    R_680(G, HIGH);
    Delay_MS(10);
    vG = ReadADC(G);
    if (vG < 1000) {
        HiZ3(S, D, G);
        return 0; //if gate is lower the vdd it meas current is flowing into it which is not MOS
    }
    vD = ReadADC(D);
    if (vD > 50) {
        HiZ3(S, D, G);
        return 0; //if the VD is higher then 0 when off means its not PMOS
    }
    R_680(G, LOW);
    Delay_MS(10);
    vG = ReadADC(G);
    if (vG > 10) {
        HiZ3(S, D, G);
        return 0; //if gate is higher then 0 it meas current is flowing into it which is not MOS
    }
    vD = ReadADC(D);
    if (vD < 900) {
        HiZ3(S, D, G);
        return 0; //if the VD is lower then 0.9*Vdd when ON means its not PMOS
    }
    HiZ3(S, D, G);
    pins[D] = 'D';
    pins[S] = 'S';
    pins[G] = 'G';
    rON = 695640 / vD;
    rON = rON - R680_IDEAL;
    return (u16) rON;
}

u16 testNPN() {
    u8 i, C[2], B[2], E[2];
    u16 Vc[2], Vb[2];
    u32 hFE[2];
    B[0] = B[1] = tList[0][0];
    C[0] = E[1] = tList[0][1];
    C[1] = E[0] = tList[1][1];
    R_470K(B[0], HIGH);
    for (i = 0; i < 2; i++) {
        R_680(C[i], HIGH);
        R_0(E[i], LOW);
        Delay_MS(10);
        Vc[i] = 1023 - ReadADC(C[i]);
        Vb[i] = 1023 - ReadADC(B[i]);
        hFE[i] = (u32) Vc[i]*691;
        hFE[i] = hFE[i] / Vb[i];
    }
    HiZ3(B[0], C[0], E[0]);
    if (hFE[0] > hFE[1])i = 0;
    else i = 1;
    pins[C[i]] = 'C';
    pins[B[i]] = 'B';
    pins[E[i]] = 'E';
    return hFE[i];
}

//reads the Vd and returns it in mV...

u16 testCA() {
    u8 A, C1, C2;
    u32 vD;
    A = tList[0][0];
    C1 = tList[0][1];
    C2 = tList[1][1];
    pins[A] = 'A';
    pins[C1] = 'C';
    pins[C2] = 'C';
    vD = tList[0][2] + tList[1][2];
    vD *= VCC_2; //((Vd1+Vd2)/2)*5000 == (Vd1+vd2)*2500
    vD = vD / 1023;
    return vD;
}

u16 testPNP() {
    u8 i, C[2], B[2], E[2];
    u16 Vc[2], Vb[2];
    u32 hFE[2];
    B[0] = B[1] = tList[0][1];
    C[0] = E[1] = tList[1][0];
    C[1] = E[0] = tList[0][0];
    R_470K(B[0], LOW);
    for (i = 0; i < 2; i++) {
        R_680(C[i], LOW);
        R_0(E[i], HIGH);
        Delay_MS(10);
        Vc[i] = ReadADC(C[i]);
        Vb[i] = ReadADC(B[i]);
        hFE[i] = (u32) Vc[i]*691;
        hFE[i] = hFE[i] / Vb[i];
    }
    HiZ3(B[0], C[0], E[0]);
    if (hFE[0] > hFE[1])i = 0;
    else i = 1;
    pins[C[i]] = 'C';
    pins[B[i]] = 'B';
    pins[E[i]] = 'E';
    return hFE[i];
}

//reads the Vd and returns it in mV...

u16 testCC() {
    u8 C, A1, A2;
    u32 vD;
    C = tList[0][1];
    A1 = tList[0][0];
    A2 = tList[1][0];
    pins[A1] = 'A';
    pins[A2] = 'A';
    pins[C] = 'C';
    vD = tList[0][2] + tList[1][2];
    vD *= VCC_2; //((Vd1+Vd2)/2)*5000 == (Vd1+vd2)*2500
    vD = vD / 1023;
    return vD;
}

u16 testDIODE() {
    u8 A, C;
    u32 v680_l, v680_h;
    u32 vD;
    A = tList[0][0];
    C = tList[0][1];

    R_680(C, LOW);
    R_680(A, HIGH);
    Delay_MS(10);
    v680_l = ReadADC(C);
    v680_h = ReadADC(A);
    HiZ(A);
    HiZ(C);

    vD = ((v680_h-v680_l) * VCC) / 1023;
    pins[A] = 'A';
    pins[C] = 'C';
    return vD;
}

u16 testTRIAC() {
    u8 A, C, G;
    if (nC < 8) {
        C = node;
        if (C)G = 0;
        else G = 2;
        A = 3 - (G + C);
    } else {
        A = node;
        if (A == 1)C = 0;
        else {
            if (A)C = 0;
            else C = 2;
        }
        G = 3 - (A + C);
    }
    pins[A] = 'A';
    pins[C] = 'C';
    pins[G] = 'G';
    if (nC < 8)return 1;
    return 2;
}

u16 testSCR() {
    u32 vG;
    u8 A, C, G;
    G = tList[0][0];
    C = tList[0][1];
    A = 3 - (C + G);
    pins[G] = 'G';
    pins[A] = 'A';
    pins[C] = 'C';
    if (tList[1][0] = G) vG = tList[1][2];
    else vG = tList[2][2];
    vG = vG * VCC;
    vG = vG / 1023;
    return vG;
}

u16 testCAP() {
    u32 Rtest;
    double cap, RC, time;
    u8 C1, C2, xC;

    //due to the way the order of testing is done tListp[0][0] can only be 0 or 1
    //C combos... 0-1,0-2,and 1-2 this is the first order of testing so pin1 and pin2 comparators are assured...
    C1 = tList[0][0];
    C2 = tList[0][1];

    //3d unused pin which could be connected to a 2nd comparator need to be brought low
    xC = 3 - (C1 + C2);
    R_0(xC, LOW);

    //safe discharege
    R_0(C2, LOW);
    R_680(C1, LOW);
    Delay_MS(100);
    R_0(C1, LOW);
    Delay_MS(10);

    //Calculate the time that capacitor needs to get to Vref=2.500V
    testCMP = 1;
    testCMPCount = 0;
    CMP_INTF = 0; //clear comparator IF
    
    T0CONbits.T08BIT = 0;     //read/write 16 bit values
    T0CONbits.T0CS = 0;       //user internal clock (Fosc/4)
    T0CONbits.PSA = 1;        //bypass prescaler
    INTCONbits.TMR0IE = 1;    //enable timer0 interrupt on overflow
    INTCONbits.TMR0IF = 0;    //clear interrupt flag

    TMR0H = 0;
    TMR0L = 0;

    CMP_INTE = 1; //enable comparator interrupt
    T0CONbits.TMR0ON = 1;      //enable timer0
    
    if (tList[0][2] > CAP_LOWCAP) {
        Rtest = R470K_IDEAL;
        R_470K(C1, HIGH);
    } else {
        Rtest = R680_IDEAL;
        R_680(C1, HIGH);
    }
    while (testCMP) {
        if (testCMPCount > 24000000){ // 2s Timeout, ~6uF for 470K, 4.1mF for 680
            CMP_INTE = 0;
            CMP_INTF = 0;
            T0CONbits.TMR0ON = 0;
            INTCONbits.TMR0IE = 0;
            INTCONbits.TMR0IF = 0;
            part_unit = 'u';
            return 0xFFFF;
        }
    }

    //safe discharege
    R_680(C1, LOW);
    Delay_MS(100);
    R_0(C1, LOW);
    Delay_MS(10);
    HiZ(C2);
    HiZ(C1);
    HiZ(xC);

    pins[C1] = 'C';
    pins[C2] = 'C';

    time = testCMPCount/12000000.0;
    RC = mylog(VCC) - mylog(VCC-2500);
    RC = RC * (double) Rtest;
    cap = (double) time / RC;
    cap = cap * 1e6;
    if (cap > 65) {
        part_unit = 'u';
    } else {
        cap = cap * 1e3;
        if (cap > 65) {
            part_unit = 'n';
        } else {
            cap = cap * 1e3;
            part_unit = 'p';
        }
    }
    return (u16) cap;
}

u16 testRES() {
    u8 R1, R2;
    u32 v0, v680, v470K, rt680, rt470K, rr;
    R1 = tList[0][0];
    R2 = tList[0][1];
    R_0(R2, LOW);
    R_680(R1, HIGH);
    Delay_MS(10);

    v0 = ReadADC(R2);
    v680 = ReadADC(R1);

    R_470K(R1, HIGH);
    Delay_MS(10);
    v470K = ReadADC(R1);
    HiZ(R1);
    HiZ(R2);

    //When Rx > sqrt(680*470000) the rt470K is more accurate
    //we calculate if this threshold has been passed below
    if (v680 > 512) rt680 = v680 - 512;
    else rt680 = 512 - v680;

    if (v470K > 512) rt470K = v470K - 512;
    else rt470K = 512 - v470K;

    if (rt470K > rt680){
        //We will use the value calculated with the 680R resistor
        if(v680==v0){ //Shortcircuit?
            rr = 1; //0 is considered error
        }
        else{
            //Use the common formula for a voltage divisor with some correction
            //The output is not 5V and 0V for HIGH and LOW, the difference increasses with
            //lower resistor values, empirically V_low=x, V_high=Vcc-2.80*V_low
            //R = 680*(v680-v0)/(1023-2.8v0-v680)
            rr = (5*R680_IDEAL*(v680-v0)) / (5*1023 - 14*v0 - 5*v680);
        }
    }
    else{
        //We will use the value calculated with the 470K resistor
        //Use the common formula for a voltage divisor
        rr = (R470K_IDEAL*v470K) / (1023 - v470K);
    }

    pins[R1] = 'R';
    pins[R2] = 'R';
    part_unit = 'R';

    if (rr > 0x03E7FC18) { //Values that overflow 16bits when divided by 1000
        rr /= 1000000;
        part_unit = 'M';
    }
    else if(rr > 0xFFFF){ //Values that overflow 16bits
            rr /= 1000;
            part_unit = 'K';
    }
    return (u16) rr;
}

u16 testZENER() {
    u8 A, C, t;
    u32 v680_l, v680_h;
    u32 vZ;

    if (tList[0][2] > tList[1][2])t = 0;
    else t = 1;

    C = tList[t][0];
    A = tList[t][1];

    R_680(A, LOW);
    R_680(C, HIGH);
    Delay_MS(10);
    v680_l = ReadADC(A);
    v680_h = ReadADC(C);
    HiZ(A);
    HiZ(C);

    vZ = ((v680_h-v680_l) * VCC) / 1023;
    pins[A] = 'A';
    pins[C] = 'C';
    return vZ;
}

u16 testDD() {
    u8 p1, p2;
    u32 vD;
    p1 = tList[0][0];
    p2 = tList[0][1];
    pins[p1] = 'D';
    pins[p2] = 'D';
    vD = tList[0][2] + tList[1][2];
    vD *= VCC_2; //((Vd1+Vd2)/2)*5000 == (Vd1+vd2)*2500
    vD = vD / 1023;
    return vD;
}

u16 testNPN_D() {
    u8 B, C, E;
    B = tList[node][0];
    E = tList[node][1];
    C = 3 - (B + E);
    pins[B] = 'B';
    pins[E] = 'E';
    pins[C] = 'C';
    return 1;
}

u16 testPNP_D() {
    u8 B, C, E;
    B = tList[node][1];
    E = tList[node][0];
    C = 3 - (B + E);
    pins[B] = 'B';
    pins[E] = 'E';
    pins[C] = 'C';
    return 1;
}


//*********************************************************************//
//          Functions to read or change pin states
//*********************************************************************//
u16 ReadADC(u8 Pin) {
    //setup ADC
    ADCON2 = 0b10101110; //R justified result, 12TAD time 101, FOSC/64 110
    //Analog inputs
    //A0-2 are analog monitors
    ADCON1 = 0b00001000; //Internal Vrefs VDD/VSS, A0-6 analog
    //set channel AN4, AN5 or AN6
    ADCON0 = ((Pin + 4) << 2); //enable the channel
    ADCON0 |= 0b00000011; //GO/!DONE=1, ADON=1
    //take reading
    while (ADCON0 & 0b10);
    return ADRES;
}

void quickADCsetup(u8 Pin) {
    //setup ADC
    ADCON2 = 0b10001110; //R justified result, 2TAD time 001, FOSC/64 110
    //Analog inputs
    //A0-2 are analog monitors
    ADCON1 = 0b00001000; //internal Vrefs VDD/VSS, A0-6 analog
    //set channel AN4, AN5 or AN6
    ADCON0 = ((Pin + 4) << 2); //enable the channel
    ADCON0 |= 0b00000001; //ADON=1 Enable A/D converter
}

void R_680(u8 Pin, u8 State) {
    u8 R0p, R680p, R470Kp;

    R0p = ((1 << Pin) << 3);
    R680p = ((1 << Pin));
    R470Kp = (1 << Pin);

    //R0 to input
    R0_DIR |= R0p;
    //470K to input
    R470K_DIR |= R470Kp;
    //680R to direction
    if (State == LOW) {
        R680_OUT &= (~(R680p));
    } else {
        R680_OUT |= ((R680p));
    }
    //680R to output
    R680_DIR &= (~(R680p));

}

void R_470K(u8 Pin, u8 State) {
    u8 R0p, R680p, R470Kp;

    R0p = ((1 << Pin) << 3);
    R680p = ((1 << Pin));
    R470Kp = (1 << Pin);

    //R0 to input
    R0_DIR |= R0p;
    //R680 to input
    R680_DIR |= (R680p);
    //470K to direction
    if (State == LOW) {
        R470K_OUT &= (~(R470Kp));
    } else {
        R470K_OUT |= (R470Kp);
    }
    //470K to output
    R470K_DIR &= (~(R470Kp));
}

void R_0(u8 Pin, u8 State) {
    u8 R0p, R680p, R470Kp;

    R0p = ((1 << Pin) << 3);
    R680p = ((1 << Pin));
    R470Kp = (1 << Pin);

    //680R to input
    R680_DIR |= (R680p);
    //470K to input
    R470K_DIR |= (R470Kp);
    //0R to direction
    if (State == LOW) {
        R0_OUT &= (~(R0p));
    } else {
        R0_OUT |= (R0p);
    }
    //0R to output
    R0_DIR &= (~(R0p));
}

void HiZ(u8 Pin) {
    u8 R0p, R680p, R470Kp;

    R0p = ((1 << Pin) << 3);
    R680p = ((1 << Pin));
    R470Kp = (1 << Pin);

    //R0 to input
    R0_DIR |= R0p;
    //680R to input
    R680_DIR |= (R680p);
    //470K to input
    R470K_DIR |= (R470Kp);

}

void HiZ3(u8 A, u8 B, u8 C) {
    HiZ(A);
    HiZ(B);
    HiZ(C);
}

//*********************************************************************//
//          Functions to output data on LCD, CDC or Serial
//*********************************************************************//
#ifdef HAS_LCD
void printPart_lcd() {
    LCD_Clear();
    LCD_CursorPosition(0);

    LCD_WriteString(type_descs[PartSS]);
    if(PartSS == ERROR || PartSS >= NOID) return;

    LCD_WriteINT(part_val);
    LCD_WriteChar(part_unit);
    LCD_CursorPosition(21);

    LCD_WriteString("1:");
    LCD_WriteChar(pins[0]);
    LCD_WriteChar(' ');

    LCD_WriteString("2:");
    LCD_WriteChar(pins[1]);
    LCD_WriteChar(' ');

    LCD_WriteString("3:");
    LCD_WriteChar(pins[2]);
    LCD_WriteChar(' ');
}
#endif //HAS_LCD

void printPart_cdc() {
    puts_cdc(type_descs[PartSS]);
    if(PartSS == ERROR || PartSS >= NOID) return;

    putINT_cdc(part_val);
    putc_cdc(part_unit);
    putc_cdc('  ');

    puts_cdc("1:");
    putc_cdc(pins[0]);
    putc_cdc(' ');

    puts_cdc("2:");
    putc_cdc(pins[1]);
    putc_cdc(' ');

    puts_cdc("3:");
    putc_cdc(pins[2]);
    putc_cdc(' ');
}

void printPart_serial() {
    putrsUSART(type_descs[PartSS]);
    if(PartSS == ERROR || PartSS >= NOID) return;

    putINT_serial(part_val);
    putcUSART(part_unit);
    putcUSART('  ');

    putrsUSART("1:");
    putcUSART(pins[0]);
    putcUSART(' ');

    putrsUSART("2:");
    putcUSART(pins[1]);
    putcUSART(' ');

    putrsUSART("3:");
    putcUSART(pins[2]);
    putcUSART(' ');
}

void tListPrint_cdc() {
    u8 i, t1;
    u16 t2;

    puts_cdc("\nnC:");
    putc_cdc(nC + '0');

    puts_cdc("\ndiff:");
    putc_cdc(diff + '0');

    for (i = 0; i < nC; i++) {
        puts_cdc("\nCP");
        putc_cdc(i + '1');
        puts_cdc(": ");

        t1 = (u8) tList[i][0];
        putc_cdc(t1 + '0');

        puts_cdc("->");

        t1 = (u8) tList[i][1];
        putc_cdc(t1 + '0');

        puts_cdc(" Value: ");
        t2 = tList[i][2];
        putINT_cdc(t2);
    }
}

void tListPrint_serial() {
    u8 i, t1;
    u16 t2;

    putrsUSART("\nVcc:");
    putINT_serial(VCC);

    putrsUSART("\nnC:");
    putcUSART(nC + '0');

    putrsUSART("\ndiff:");
    putcUSART(diff + '0');
    
    for (i = 0; i < 12; i++) {
        putrsUSART("\nCP");
        putcUSART(i + '1');
        putrsUSART(": ");

        t1 = (u8) tList[i][0];
        putcUSART(t1 + '0');

        putrsUSART("->");

        t1 = (u8) tList[i][1];
        putcUSART(t1 + '0');

        putrsUSART(" Value: ");
        t2 = tList[i][2];
        putINT_serial(t2);
    }

    for (i = 0; i < 3; i++) {
        putrsUSART("\ntIN");
        putcUSART(i + '1');
        putrsUSART(": ");
        putcUSART(tIN[i] + '0');
    }

    for (i = 0; i < 3; i++) {
        putrsUSART("\ntOUT");
        putcUSART(i + '1');
        putrsUSART(": ");
        putcUSART(tOUT[i] + '0');
    }

    for (i = 0; i < 3; i++) {
        putrsUSART("\ntC");
        putcUSART(i + '1');
        putrsUSART(": ");
        putcUSART(tC[i] + '0');
    }

    for (i = 0; i < 8; i++) {
        putrsUSART("\ntN");
        putcUSART(i + '1');
        putrsUSART(": ");
        putcUSART(tN[i] + '0');
    }
}

//*********************************************************************//
//          Helper functions
//*********************************************************************//
void Delay_MS(u8 ms) {
    static u16 timer;
    u8 i;

    for (i = 0; i < ms; i++) {
        timer = 6000;
        while (timer--);
    }

}

void initT() {
    u8 i, j;
    for (i = 0; i < 3; i++) {
        tIN[i] = 0;
        tOUT[i] = 0;
        tC[i] = 0;
    }
    for (i = 0; i < 8; i++) tN[i] = 0;

}

void putcUSART(char data){
    while(PIR1bits.TXIF == 0);
    TXREG = data;
}

void putrsUSART(const rom char *data){
     while(*data){
         // Transmit a byte
         while(PIR1bits.TXIF == 0);
         TXREG = *data++;
     }
 }

void puts_cdc(rom char *s) {
    char c;
    while ((c = *s++)) putc_cdc(c);
}

void putINT_cdc(u16 vcdc) {
    unsigned int temp, dvd = 10000;
    char i = 0, k = 0;
    temp = vcdc;
    CDC_Flush_In_Now();
    for (i = 0; i < 5; i++) {
        temp = vcdc / dvd;
        if (temp) k++;
        if (k) putc_cdc((char) temp + '0');
        vcdc = vcdc - (temp * dvd);
        dvd /= 10;
    }
    CDC_Flush_In_Now();
}

void putINT_serial(u16 vcdc) {
    unsigned int temp, dvd = 10000;
    char i = 0, k = 0;
    temp = vcdc;
    for (i = 0; i < 5; i++) {
        temp = vcdc / dvd;
        if (temp) k++;
        if (k) putcUSART((char) temp + '0');
        vcdc = vcdc - (temp * dvd);
        dvd /= 10;
    }
}

double mylog(u16 value){
    u8 integer_p = 0;
    u16 value_tmp = value;
    u32 temp32;
    double tempD;
    u8 i;


    //To calculate the log2(x) whe get the integer part first [N]
    while (value_tmp>>=1)
        integer_p++;

    //The fractional part is obtained iterating, the start value is x/(2^N)
    value_tmp = 1;
    value_tmp <<= integer_p;
    tempD = (double)value/(double)value_tmp;

    //Each iteration gives 1bit more of resolution, if the square of our value is
    //bigger than 2 the bit is 1 and be divide by 2, otherwise the bit is 0
    temp32 = 0;
    for(i=0;i<24;i++){
        tempD *= tempD;
        temp32<<=1;
        if (tempD>2){
            temp32|=1;
            tempD /= 2.0;
        }
    }

    //Divide by the bits of resolution to obtailn the fractional part
    tempD = (double)temp32/16777216.0;
    //Add the integer part and we have the result of log2(x)
    tempD += integer_p;

    //The ln is calculated as ln(x)=log2(x)/log2(e)
    return (tempD * 0.6931471805599453);
}
/////////////////EOF
