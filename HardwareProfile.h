#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

//Hardware defines
#define R0_DIR	TRISB
#define R0_OUT	LATB
#define R0_IN	PORTB

#define R680_DIR TRISD
#define R680_OUT LATD

#define R470K_DIR TRISB
#define R470K_OUT LATB

#define CMP_INTE PIE2bits.CMIE
#define CMP_INTF PIR2bits.CMIF

#define R680_IDEAL  680
//#define R680_0  686
//#define R680_1  691
//#define R680_2  707

#define R470K_IDEAL  470000
//#define R470K_0  465000
//#define R470K_1  467000
//#define R470K_2  465000

//#define HAS_LCD            //To use LCD
#define HAS_SERIAL_PORT     //To use TX serial to debug
#define HAS_USB_CDC         //To enable CDC over USB

#if defined(HAS_LCD) && defined(HAS_SERIAL_PORT)
    #error "LCD and serial can not be enabled at the same time"
#endif

#endif 
